import pygame

from engine import Animation, GameObject, Warrior
from player import Player

pygame.init()

screen = pygame.display.set_mode((1200, 720))

# game_icon = pygame.image.load()
background_image = pygame.image.load('images/45908.jpg')

player_idle_anim = Animation([
    pygame.transform.scale(pygame.image.load('images/character/character_01.png'), (60, 140)),
])

player_walk_anim = Animation([
    pygame.transform.scale(pygame.image.load('images/character/character_01.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_02.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_03.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_04.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_05.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_06.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_07.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_08.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_09.png'), (60, 140))
])

player_attack_anim = Animation([
    pygame.transform.scale(pygame.image.load('images/character/character_11.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_12.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_13.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_14.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_15.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_16.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_17.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_18.png'), (60, 140))
], False)

warrior_idle_anim = Animation([
    pygame.transform.scale(pygame.image.load('images/character/character_01.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_01.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_01.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_01.png'), (60, 140)),
])

warior_walk_anim = Animation([
    pygame.transform.scale(pygame.image.load('images/character/character_01.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_02.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_03.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_04.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_05.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_06.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_07.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_08.png'), (60, 140)),
    pygame.transform.scale(pygame.image.load('images/character/character_09.png'), (60, 140))
])


class GameController:
    game_title = 'My Game'
    running = True

    def __init__(self):
        pygame.display.set_caption(self.game_title)

    def game_running(self):
        my_player = Player((200, 450), {'idle': player_idle_anim,
                                        'walk': player_walk_anim,
                                        'attack': player_attack_anim})
        warrior1 = Warrior((600, 450), {'idle': warrior_idle_anim, 'walk': warior_walk_anim})
        while self.running:
            warriors = [warrior1]
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        my_player.check_attack_range(warriors)
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RIGHT:
                        my_player.walk('right')
                    if event.key == pygame.K_LEFT:
                        my_player.walk('left')
                elif event.type == pygame.KEYUP:
                    my_player.idle()
            screen.blit(background_image, (0, 0))
            warrior1.draw(screen)
            my_player.draw(screen)
            pygame.display.update()


game_controller = GameController()
game_controller.game_running()
