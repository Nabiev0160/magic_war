import pygame


class Animation:
    animation_pos = 0
    see_side = 'right'
    anim_end = False

    def __init__(self, images, loop=True):
        self.images = images
        self.loop = loop

    def _add_animation_pos(self):
        anim_end = False
        if self.loop and not len(self.images) - 1 == self.animation_pos:
            self.animation_pos += 1
        elif not self.loop:
            if len(self.images) - 1 == self.animation_pos:
                anim_end = True
            else:
                self.animation_pos += 1
        else:
            self.animation_pos = 0
        return anim_end

    def draw_animation(self, position, screen):
        if self.see_side == 'right':
            screen.blit(self.images[int(self.animation_pos / 2)], position)
        else:
            screen.blit(pygame.transform.flip(self.images[int(self.animation_pos / 2)], True, False), position)
        return self._add_animation_pos()


    def set_side(self, side):
        self.see_side = side


class GameObject:
    now_animation = 'idle'

    def __init__(self, position, animations):
        self.pos_x, self.pos_y = position
        self.animations = animations

    def idle(self):
        self.now_animation = 'idle'

    def draw(self, screen):
        self.animations[self.now_animation].draw_animation((self.pos_x, self.pos_y), screen)


class Warrior(GameObject):
    hP = 100
    armour = 10
    see_side = 'left'
    dead_status = False

    def _warrior_walking(self):
        if self.see_side == 'right':
            self.pos_x += .5
            self.see_side = 'right'
        elif self.see_side == 'left':
            self.pos_x -= .5
            self.see_side = 'left'

    def walk(self, axis):
        self.now_animation = 'walk'
        self.see_side = axis
        self._warrior_walking()

    def check_dead(self):
        if self.hP <= 0:
            self.dead_status = True

    def idle(self):
        self.now_animation = 'idle'

    def draw(self, screen):
        if not self.dead_status:
            if self.see_side == 'right':
                for elem in self.animations.values():
                    elem.see_side = 'right'
            elif self.see_side == 'left':
                for elem in self.animations.values():
                    elem.see_side = 'left'
            if self.now_animation == 'walk':
                self._warrior_walking()
            self.animations[self.now_animation].draw_animation((self.pos_x, self.pos_y), screen)