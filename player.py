import pygame

from engine import GameObject


class Player(GameObject):
    now_animation = 'idle'
    see_side = 'right'
    damage = 10
    hP = 100
    armour = 10

    def _player_walking(self):
        if self.see_side == 'right':
            for elem in self.animations.values():
                elem.see_side = 'right'
            self.pos_x += .5
            self.see_side = 'right'
        elif self.see_side == 'left':
            for elem in self.animations.values():
                elem.see_side = 'left'
            self.pos_x -= .5
            self.see_side = 'left'

    def _attack(self, other_objects):
        other_objects.hP -= self.damage

    def check_attack_range(self, warriors):
        self.now_animation = 'attack'
        for warrior in warriors:
            if self.see_side == 'right' and warrior.pos_x - self.pos_x <= 200 and warrior.pos_x - self.pos_x > 0:
                self._attack(warrior)
                warrior.check_dead()
                print(warrior.hP)
                print('right')
            if self.see_side == 'left' and self.pos_x - warrior.pos_x <= 200 and self.pos_x - warrior.pos_x > 0:
                self._attack(warrior)
                warrior.check_dead()
                print(warrior.hP)
                print('left')


    def walk(self, axis):
        self.now_animation = 'walk'
        self.see_side = axis
        self._player_walking()

    def idle(self):
        self.now_animation = 'idle'

    def draw(self, screen):
        if self.now_animation == 'walk':
            self._player_walking()
        anim_end_status = self.animations[self.now_animation].draw_animation((self.pos_x, self.pos_y), screen)
        if anim_end_status:
            self.now_animation = 'idle'
